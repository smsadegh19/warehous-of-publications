FROM dockerhub.ir/python:3.10-slim-buster AS base

ENV PYTHONUNBUFFERED 1

RUN mkdir /opt/warehous
WORKDIR /opt/warehous


RUN apt-get update && apt-get install -y vim curl net-tools iputils-ping telnet \
    libpq-dev g++ gcc gettext git gdal-bin gnupg \
    && curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - && \
    curl https://packages.microsoft.com/config/debian/10/prod.list > /etc/apt/sources.list.d/mssql-release.list && \
    apt-get update && ACCEPT_EULA=Y apt-get install -y default-libmysqlclient-dev

COPY requirements.txt /opt/warehous/requirements.txt
RUN pip install --upgrade pip && pip install --no-cache-dir -r requirements.txt

FROM base AS build

COPY ./warehous /opt/warehous
RUN mkdir /opt/warehous/logs

RUN ["./manage.py", "collectstatic", "--no-input"]

RUN chmod +x /opt/warehous/entrypoint.sh
CMD ["./entrypoint.sh"]

FROM dockerhub.ir/nginx:1.17.6 AS static

COPY --from=build /opt/warehous/static /usr/share/nginx/html/static