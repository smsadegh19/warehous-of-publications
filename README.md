# warehouse_of_publications

## Team members:
|First Name|Last Name|Student ID|
|---|---|---|
|Mohammadsadegh|Salimi|97101829|
|Erfan|Moeini|97110206|
|Mohammadhossein|Bahmani|97105811|
|Mohammadhosein|Gheisarieh|97106238|

## How to run w/o docker:
```
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
python manage.py migrate
python manage.py runserver
```
